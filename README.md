# Project #
* OSM HOT Validation Service

### What is this repository for? ###
* HOT validation service. Extracting validation data from HOT tasking manager and uploading to Oracle/PostgreSQL using FME.
* Validating data using 1Integrate for QGIS (Plugin can be downloaded https://bitbucket.org/Augustin_Gagnon/qgis_integrate)

This is a proof of concept research project and has not been officially released.

### Who do I talk to? ###
* Steve Penson: Consulting Cambridge