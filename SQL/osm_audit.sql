set SERVEROUTPUT on

begin
   execute immediate 'DROP TABLE my_results';
exception
   when others then
      if sqlcode != -942 then
         raise;
      end if;
end;
/

create table my_results (
  table_name varchar2(30),
  column_name varchar2(30),
  row_count integer);

declare
  l_sql varchar2(4000);
  l_count integer;
begin

  for rec in (
    select table_name, column_name
    from user_tab_columns
    where 
      (table_name = 'POINTS'
      or table_name = 'LINES'
      or table_name = 'MULTIPOLYGONS')
      and column_name != 'OSM_ID'
      and column_name != 'OSM_WAY_ID'
      and column_name != 'GEOM'
      and column_name != 'Z_INDEX'
      
    order by 1)
  loop
      
    l_sql := 'select count(*) from ' || rec.table_name || ' where "' || rec.column_name || '" is not null';
    execute immediate l_sql into l_count;
    
    insert into my_results (table_name, column_name, row_count) 
    values (rec.table_name, rec.column_name, l_count);
   
    dbms_output.put_line('table ' || rec.table_name || ' column ' || rec.column_name || ' row_count ' || l_count);
    
  end loop;
  commit;
end;
/

select * 
from my_results
order by 1,3 desc;